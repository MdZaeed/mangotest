﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using MongoDB.Driver;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class VehiclesController : ApiController
    {
        static MongoDatabase db1 = new MongoContext().GetDb();
        // GET: api/Vehicles
/*
        [EnableCors(origins: "*", headers: "*", methods: "*")]
*/
        public IQueryable<VehicleView> GetVehicles()
        {
            if (Tools.GetUserType(Request.Headers) != "Admin" && Tools.GetUserType(Request.Headers) != "User")
            {
                return null;
            }

            var usercollection = db1.GetCollection<UserTemp>("UserCollection");
            var favo = usercollection.FindAll().ToList();
            var favs =
                favo.Where(us => us.UserId == Tools.GetUserId(Request.Headers)).Select(us => us.FavoriteIds).ToList();
            var favst = favs.FirstOrDefault();

            var collection = db1.GetCollection<Vehicle>("VehicleCollection");
            var universityCollection = db1.GetCollection<University>("UniversityCollection");
            List<VehicleView> vehiclesWithUniversityName=new List<VehicleView>();
            foreach(Vehicle vehicle in collection.FindAll().ToList())
            {
                VehicleView tempVehicle = vehicle.AssignWithViewModel();
                var collLis = universityCollection.FindAll().ToList();
                var foo = collLis.Where(uni => uni.UniversityId == vehicle.UniversityId).Select(uni => uni).ToList();
                tempVehicle.UniversityName = foo[0].UniversityName;
                if (favst != null && favst.Contains(vehicle.VehicleId))
                {
                    tempVehicle.IsFavorite = "true";
                }
                else
                {
                    tempVehicle.IsFavorite = "false";
                }
                vehiclesWithUniversityName.Add(tempVehicle);
            }
            return vehiclesWithUniversityName.AsQueryable();
        }

        // GET: api/Vehicles
//        [EnableCors(origins: "*", headers: "*", methods: "*")]
//        [Route("api/test"), HttpGet()]
/*        [HttpGet]
        public IQueryable<VehicleView> Test()
        {
            var collection = db1.GetCollection<Vehicle>("VehicleCollection");
            var universityCollection = db1.GetCollection<University>("UniversityCollection");
            var vehiclesWithUniversityName = new List<VehicleView>();
            foreach (Vehicle vehicle in collection.FindAll().ToList())
            {
                VehicleView tempVehicle = vehicle.AssignWithViewModel();
                var collLis = universityCollection.FindAll().ToList();
                var foo = collLis.Where(uni => uni.UniversityId == vehicle.UniversityId).Select(uni => uni).ToList();
                tempVehicle.UniversityName = foo[0].UniversityName;
                vehiclesWithUniversityName.Add(tempVehicle);
            }
            return vehiclesWithUniversityName.AsQueryable();
        }*/

        // GET: api/Vehicles/5
        [ResponseType(typeof(Vehicle))]
        public Vehicle GetVehicle(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin" && Tools.GetUserType(Request.Headers) != "User")
            {
                return null;
            }
            var collection = db1.GetCollection<Vehicle>("VehicleCollection");
            var universityCollection = db1.GetCollection<University>("UniversityCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => veh1.VehicleId == id).Select(beh1 => beh1).ToList();
            Vehicle vehicle=vu[0];
            VehicleView tempVehicle = vehicle.AssignWithViewModel();
            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityId == vehicle.UniversityId).Select(uni => uni).ToList();
            tempVehicle.UniversityName = foo[0].UniversityName;

            return vehicle;
        }

        // POST: api/Vehicles
        [ResponseType(typeof(Vehicle))]
        public IHttpActionResult PostVehicle(VehicleView vehicleView)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var collection = db1.GetCollection<Vehicle>("VehicleCollection");

            var tt = collection.FindAll().ToList();
            var gg = tt[tt.Count - 1].VehicleId;

            Vehicle vehicle = TransferVehicleViewToVehicle(vehicleView, gg);
            collection.Insert(vehicle);

            return CreatedAtRoute("DefaultApi", new { id = vehicle.VehicleId }, vehicle);
        }

        public Vehicle TransferVehicleViewToVehicle(VehicleView vehicleView, String lastElementId)
        {
            Vehicle vehicle = vehicleView.TransformToVehicle();

            var universityCollection = db1.GetCollection<University>("UniversityCollection");

            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityName == vehicleView.UniversityName).Select(uni => uni).ToList();

            vehicle.UniversityId = foo[0].UniversityId;
            int temp;
            int.TryParse(lastElementId, out temp);
            vehicle.VehicleId =( temp + 1) +"";

            return vehicle;
        }

        public static Vehicle StaticTransferVehicleViewToVehicle(VehicleView vehicleView, String lastElementId)
        {
            Vehicle vehicle = vehicleView.TransformToVehicle();

            var universityCollection = db1.GetCollection<University>("UniversityCollection");

            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityName == vehicleView.UniversityName).Select(uni => uni).ToList();

            vehicle.UniversityId = foo[0].UniversityId;
            int temp;
            int.TryParse(lastElementId, out temp);
            vehicle.VehicleId = (temp + 1) + "";

            return vehicle;
        }

        // DELETE: api/Vehicles/5
        [ResponseType(typeof(Vehicle))]
        public void DeleteVehicle(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return;
            }
            var collection = db1.GetCollection<Vehicle>("VehicleCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => veh1.VehicleId != id).Select(beh1 => beh1).ToList();

            collection.Drop();

            foreach (Vehicle vehicle in vu)
            {
                collection.Insert(vehicle);
            }
        }

        public static String SaveFromAdminPanel(VehicleView vehicleView)
        {
            var collection = db1.GetCollection<Vehicle>("VehicleCollection");

            var tt = collection.FindAll().ToList();
            var gg = tt[tt.Count - 1].VehicleId;

            Vehicle vehicle = StaticTransferVehicleViewToVehicle(vehicleView, gg);
            collection.Insert(vehicle);

            return "true";
        }
    }
}