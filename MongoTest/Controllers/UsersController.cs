﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using MongoDB.Driver;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class UsersController : ApiController
    {
        private readonly MongoDatabase _db1 = new MongoContext().GetDb();

        // GET: api/Users
        public IQueryable<UserViewModel> GetUsers()
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            List<UserViewModel> userViewModels=new List<UserViewModel>();
            foreach (UserTemp user in collection.FindAll().ToList())
            {
                userViewModels.Add(UserViewModel.Transform(user));
            }
            return userViewModels.AsQueryable();
        }

        // GET: api/Users/5
        [ResponseType(typeof(UserViewModel))]
        public UserViewModel GetUser(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            var ui = collection.FindAll().ToList();
            var uu = ui.Where(use1 => use1.UserId == id).Select(use1 => use1).ToList();
            UserViewModel userView = UserViewModel.Transform(uu[0]);

            return userView;
        }


        // DELETE: api/Users/5
        [ResponseType(typeof(UserTemp))]
        public void DeleteUser(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return;
            }
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => veh1.UserId != id).Select(beh1 => beh1).ToList();

            collection.Drop();

            foreach (UserTemp user in vu)
            {
                collection.Insert(User);
            }
        }

/*        [Route("api/headertest")]
        [HttpGet]
        public LoginResultModel HeaderTest()
        {
            IEnumerable<string> headervalues;
            Request.Headers.TryGetValues("Auth-key", out headervalues);
            LoginResultModel loginResultModel = new LoginResultModel
            {
                Success = "True",
                Message = headervalues.FirstOrDefault()
            };

            return loginResultModel;
        }*/
    }
}