﻿using MongoDB.Driver;
using MongoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace MongoTest.Controllers
{
    public class UniversityController : ApiController
    {
        private static readonly MongoDatabase _db1 = new MongoContext().GetDb();

        // GET: api/Universities
        public IQueryable<University> GetUniversities()
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            var collection = _db1.GetCollection<University>("UniversityCollection");
            return collection.FindAll().ToList().AsQueryable();
            // return db.Vehicles;
        }


        // GET: api/Vehicles/5
        [ResponseType(typeof(University))]
        public University GetUniversity(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            var universityCollection = _db1.GetCollection<University>("UniversityCollection");
            var vu = universityCollection.FindAll().ToList();
            var re = vu.Where(uni1 => uni1.UniversityId == id).Select(uni1 => uni1).ToList();
            return re[0];
        }

        // POST: api/Vehicles
        [ResponseType(typeof(University))]
        public IHttpActionResult PostUniversity(University university)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return null;
            }
            var universityCollection = _db1.GetCollection<University>("UniversityCollection");
            var vu = universityCollection.FindAll().ToList();
            var va = vu[vu.Count - 1];
            int temp = int.Parse(va.UniversityId) + 1;
            university.UniversityId = temp + "";
            universityCollection.Insert(university);

            return CreatedAtRoute("DefaultApi", new { id = university.UniversityId }, university );
        }

        // DELETE: api/Vehicles/5
        [ResponseType(typeof(University))]
        public void DeleteUniversity(string id)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin")
            {
                return;
            }
            var universityCollection = _db1.GetCollection<University>("UniversityCollection");
            var ve = universityCollection.FindAll().ToList();
            var vu = ve.Where(uni1 => uni1.UniversityId != id).Select(uni1 => uni1).ToList();

            universityCollection.Drop();

            foreach (University university in vu)
            {
                universityCollection.Insert(university);
            }
        }

        public static String AdminPost(University university)
        {
            var universityCollection = _db1.GetCollection<University>("UniversityCollection");
            var vu = universityCollection.FindAll().ToList();
            var va = vu[vu.Count - 1];
            int temp = int.Parse(va.UniversityId) + 1;
            university.UniversityId = temp + "";
            universityCollection.Insert(university);

            return "true";
        }
    }
}
