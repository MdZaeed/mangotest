﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class RemovefavoritesController : ApiController
    {
        private readonly MongoDatabase _db1 = new MongoContext().GetDb();

        public LoginResultModel Post(VehiclePostModel vehiclePostModel)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin" && Tools.GetUserType(Request.Headers) != "User")
            {
                return CreateResultModel("false", "Wrong Authentication Data");
            }

            var usercollection = _db1.GetCollection<UserTemp>("UserCollection");
            var favo = usercollection.FindAll().ToList();
            var favs = favo.Where(us => us.UserId == Tools.GetUserId(Request.Headers)).Select(us => us.FavoriteIds).ToList();
            var favst = favs.FirstOrDefault();

            var vehicleId = vehiclePostModel.Vehicleid;

            if (favst == "")
            {
                return CreateResultModel("false", "No favorites in the list");
            }
            else
            {
                if (favst != null && favst.Contains(vehicleId))
                {
                    favst = CutTheString(favst, vehicleId);
                }
                else
                {
                    return CreateResultModel("false", "Not included in favorite list");
                }
            }

            usercollection.Update(Query.EQ("UserId", Tools.GetUserId(Request.Headers)), Update.Set("FavoriteIds", favst));

            return CreateResultModel("true", "Successfully Added to Favorites");
        }

        public LoginResultModel CreateResultModel(string success, string message)
        {
            LoginResultModel loginResultModel = new LoginResultModel
            {
                Success = success,
                Message = message
            };

            return loginResultModel;
        }

        public string CutTheString(string originalString,string toBeCut)
        {
            string reducedString=null;
            char[] delimChars = {','};
            String[] parts = originalString.Split(delimChars);

            if (parts.Length == 1 && originalString.Equals(toBeCut))
            {
                reducedString = "";
            }
            else
            {
                reducedString = "";
                
                foreach (string part in parts)
                {
                    if (!part.Equals(toBeCut))
                    {
                        reducedString = reducedString + part + ",";
                    }
                }
                reducedString = reducedString.Substring(0, reducedString.Length - 1);
            }
            return reducedString;
        }
    }
}
