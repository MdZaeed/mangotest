﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MongoDB.Driver;
using MongoTest.Models;
using Newtonsoft.Json;

namespace MongoTest.Controllers
{
    public class LocationController : ApiController
    {
        private readonly MongoDatabase _db1 = new MongoContext().GetDb();

        // GET: api/Universities
        public IHttpActionResult GetLocation()
        {
            var collection = _db1.GetCollection<Location>("LocationCollection");
            return Json(collection.FindAll().ToList().AsQueryable());
            // return db.Vehicles;
        }

        // GET: api/Vehicles/5
        [ResponseType(typeof(Location))]
        public IQueryable<Location> GetLocation(string id)
        {
            var collection = _db1.GetCollection<Vehicle>("VehicleCollection");
            var universityCollection = _db1.GetCollection<University>("UniversityCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => veh1.VehicleId == id).Select(beh1 => beh1).ToList();
            Vehicle vehicle = vu[0];
            VehicleView tempVehicle = vehicle.AssignWithViewModel();
            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityId == vehicle.UniversityId).Select(uni => uni).ToList();
            tempVehicle.UniversityName = foo[0].UniversityName;

            if (vehicle.LoactorType.Equals("Device") || vehicle.LoactorType.Equals("AndroidLive"))
            {
                var locationToSend=new List<Location>();
                var request = (HttpWebRequest)WebRequest.Create("http://www.trimarkworld.com/zayed/getLocation.php?id=" + id);

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(stream: response.GetResponseStream()).ReadToEnd();
                var item = JsonConvert.DeserializeObject<LocationRealGetRsponse>(responseString);
                foreach(LocationGetModel locationGetModel in item.locations)
                {
                    if (locationGetModel.latitude.Equals("0") || locationGetModel.longitude.Equals("0"))
                    {
                        continue;
                    }
                    if (vehicle.LoactorType.Equals("Device"))
                    {
                        locationToSend.Add(locationGetModel.TransformWithLatLngTransformationLocation());
                    }
                    else
                    {
                        locationToSend.Add(locationGetModel.TransformToLocation());                    
                    }
                    break;
                }

                return locationToSend.AsQueryable();
            }
            var locationCollection = _db1.GetCollection<Location>("LocationCollection");
            var loca1 = locationCollection.FindAll().ToList();
            var re = loca1.Where(uni1 => uni1.VehicleId == id).Select(uni1 => uni1).ToList();
            return re.AsQueryable();
        }

/*        [HttpGet]
        [ResponseType(typeof(Location))]
        [Route ("api/getLocationbyvehicleid/{id}")]
        public IQueryable<Location> GetLocationByVehicleId(int id)
        {
            var universityCollection = _db1.GetCollection<Location>("LocationCollection");
            var vu = universityCollection.FindAll().ToList();
            var re = vu.Where(uni1 => uni1.VehicleId == id + "").Select(uni1 => uni1).ToList().AsQueryable();
            return re;
        }*/

        // POST: api/Vehicles
        [ResponseType(typeof(Location))]
        public IHttpActionResult PostLocation(Location location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var collection = _db1.GetCollection<Location>("LocationCollection");

            var tt = collection.FindAll().ToList();
            var gg = int.Parse(tt[tt.Count - 1].LocationId);
            location.LocationId = (gg + 1) + "";

            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(2010, 1, 1))).TotalSeconds;
            location.Time = unixTimestamp + "";


            collection.Insert(location);

            return CreatedAtRoute("DefaultApi", new { id = location.LocationId }, location);
        }

        // DELETE: api/Vehicles/5
        [ResponseType(typeof(Location))]
        public void DeleteLocation(string id)
        {
            var collection = _db1.GetCollection<Location>("LocationCollection");

            var ve = collection.FindAll().ToList();
            var vu = ve.Where(loc1 => loc1.LocationId != id).Select(loc1 => loc1).ToList();

            collection.Drop();

            foreach (Location location in vu)
            {
                collection.Insert(location);
            }
        }

    }
}
