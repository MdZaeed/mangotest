﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Web.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class AddfavoritesController : ApiController
    {
        private readonly MongoDatabase _db1 = new MongoContext().GetDb();

        public LoginResultModel Post(VehiclePostModel vehiclePostModel)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin" && Tools.GetUserType(Request.Headers) != "User")
            {
                return CreateResultModel("false","Wrong Authentication Data");
            }

            var usercollection = _db1.GetCollection<UserTemp>("UserCollection");
            var favo = usercollection.FindAll().ToList();
            var favs =
                favo.Where(us => us.UserId == Tools.GetUserId(Request.Headers)).Select(us => us.FavoriteIds).ToList();
            var favst = favs.FirstOrDefault();

            var vehicleId = vehiclePostModel.Vehicleid;

            if (favst == "")
            {
                favst = vehicleId;
            }
            else
            {
                if (favst != null && !favst.Contains(vehicleId))
                {
                    favst = favst + "," + vehicleId;
                }
                else
                {
                    return CreateResultModel("false", "Already Added To Favorites");
                }
            }

            usercollection.Update(Query.EQ("UserId",Tools.GetUserId(Request.Headers)),Update.Set("FavoriteIds",favst));

            return CreateResultModel("true","Successfully Added to Favorites");
        }

        public LoginResultModel CreateResultModel(string success,string message)
        {
            LoginResultModel loginResultModel=new LoginResultModel
            {
                Success = success,
                Message = message
            };

            return loginResultModel;
        }
    }
}
