﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoTest.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MongoTest.Controllers
{
    public class HomeController : Controller
    {
        public MongoDatabase Db;

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";


/*            var obj = JObject.Parse(responseString);

            foreach (KeyValuePair<String, JToken> app in obj)
            {
                var appName = app.Key;
                var du=(JArray)app.Value[appName];
//                var description = JArray.Parse((String) app.Value[appName]);
                var x = 0;

            }*/

/*            var objects = JObject.Parse(responseString);

            dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
            string Text = stuff1.;
            Console.WriteLine(Text);*/

            Db = new MongoContext().GetDb();

            var collection = Db.GetCollection<Vehicle>("VehicleCollection");
            collection.Drop();

            Vehicle vehicle = new Vehicle
            {
                VehicleId = "1",
                DriverName = "Md. Rudra",
                ActiveTime = "9-12",
                Routes = "Kalabagan,Dhanmondi",
                UniversityId = "1",
                RouteCoords = "23.728158,90.403263;23.732696,90.395216;23.732401,90.385088;23.764137,90.370809",
                LoactorType = "Device"
            };

            collection.Save(vehicle);

            vehicle = new Vehicle
            {
                VehicleId = "2",
                DriverName = "Shamim",
                ActiveTime = "12-15",
                Routes = "Shyamoli,Gabtoli",
                UniversityId = "1",
                RouteCoords = "23.728033,90.400648;23.727581,90.409253;23.737045,90.410299",
                LoactorType = "AndroidLocal"

            };

            collection.Insert(vehicle);

            vehicle = new Vehicle
            {
                VehicleId = "3",
                DriverName = "Fuad",
                ActiveTime = "9-12",
                Routes = "Mugda,Motijhil",
                UniversityId = "2",
                RouteCoords = "23.728158,90.403263;23.732696,90.395216;23.732401,90.385088;23.764137,90.370809",
                LoactorType = "AndroidLocal"

            };

            collection.Insert(vehicle);

            vehicle = new Vehicle
            {
                VehicleId = "4",
                DriverName = "Shaon ",
                ActiveTime = "9-12",
                Routes = "Khulna,Rajshahi",
                UniversityId = "2",
                RouteCoords = "23.728158,90.403263;23.732696,90.395216;23.732401,90.385088;23.764137,90.370809",
                LoactorType = "AndroidLocal"

            };

            collection.Insert(vehicle);

            vehicle = new Vehicle
            {
                VehicleId = "5",
                DriverName = "Misu Be Imp",
                ActiveTime = "19-24",
                Routes = "Kalabagan,Dhanmondi",
                UniversityId = "1",
                RouteCoords = "23.728033,90.400648;23.727581,90.409253;23.737045,90.410299",
                LoactorType = "AndroidLive"
            };

            collection.Insert(vehicle);

            vehicle = new Vehicle
            {
                VehicleId = "6",
                DriverName = "Gangua ",
                ActiveTime = "9-12",
                Routes = "Kalabagan,Dhanmondi",
                UniversityId = "2",
                RouteCoords = "23.728158,90.403263;23.732696,90.395216;23.732401,90.385088;23.764137,90.370809",
                LoactorType = "AndroidLive"
            };

            collection.Insert(vehicle);


            /*
            var collection1 = Db.GetCollection<University>("UniversityCollection");
            collection1.Drop();

            collection1.Drop();

            University university = new University
            {
                UniversityId = "1",
                UniversityName = "University of Dhaka"
            };

            collection1.Insert(university);

            university = new University
            {
                UniversityId = "2",
                UniversityName = "Jahangirnagar University"
            };

            collection1.Insert(university);

            var collection2 = Db.GetCollection<University>("LocationCollection");
            collection2.Drop();


            /*
                                    collection2.Drop();
            #1#

            Int32 unixTimestamp = (Int32) (DateTime.UtcNow.Subtract(new DateTime(2010, 1, 1))).TotalSeconds;

            Location location = new Location
            {
                LocationId = "1",
                Longitude = "90.3925",
                Latitude = "23.7315",
                VehicleId = "1",
                Time = (unixTimestamp - 10) + ""
            };

            collection2.Insert(location);

            location = new Location
            {
                LocationId = "2",
                Longitude = "35",
                Latitude = "80",
                VehicleId = "5",
                Time = (unixTimestamp - 5) + ""
            };

            collection2.Insert(location);

            location = new Location
            {
                LocationId = "3",
                Longitude = "30",
                Latitude = "70",
                VehicleId = "3",
                Time = (unixTimestamp - 2) + ""
            };

            collection2.Insert(location);

            var collection3 = Db.GetCollection<UserTemp>("UserCollection");
            collection3.Drop();


            /*
                        collection3.Drop();
            #1#

            UserTemp user = new UserTemp
            {
                FullName = "Dipok Chandra Das",
                UniversityId = "2",
                Department = "IIT",
                Institute = "A unit",
                UserId = "1",
                FavoriteIds = "1,4,5",
                Usertype = "User",
                Username = "dipok0501",
                Password = "153045609z",
                Email = "dipok0501@gmail.com",
                Fid = "7765"
            };

            collection3.Save(user);

            user = new UserTemp
            {
                FullName = "Mohammad Zaeed",
                UniversityId = "1",
                Department = "IIT",
                Institute = "A unit",
                UserId = "2",
                FavoriteIds = "2,3,5",
                Usertype = "Admin",
                Username = "zayed0504",
                Password = "153045609z",
                Email = "zayed0504@gmail.com",
                Fid = "7766"
            };

            collection3.Insert(user);

            user = new UserTemp
            {
                FullName = "Md. Abdul Mannan",
                UniversityId = "2",
                Department = "Fishries",
                Institute = "Biology",
                UserId = "3",
                FavoriteIds = "2,3,4",
                Usertype = "User",
                Username = "mannan0522",
                Password = "153045609z",
                Email = "mannan22@gmail.com",
                Fid = "7767"
            };

            collection3.Insert(user);*/

            return View();
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult Login()
        {
            ViewBag.Title = "Login";

            return View();
        }

        [System.Web.Mvc.HttpPost]
        public RedirectToRouteResult Login(String firstname,String lastname)
        {
            String result=LoginController.Receive(firstname, lastname);
            if (result.Equals("true"))
            {
                return RedirectToAction("AdminHome");
            }
            else
            {
                return null;
            }
        }

        public ActionResult AdminHome()
        {
            return View();
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult AddVehicle()
        {
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult AddVehicle(String drivername,String routes,String triptime,String uniname,String routecoords)
        {
            VehicleView vehicleView=new VehicleView
            {
                DriverName = drivername,
                Routes = routes,
                ActiveTime = triptime,
                UniversityName = uniname,
                RouteCoords = routecoords
            };

            String result = VehiclesController.SaveFromAdminPanel(vehicleView);
            return RedirectToAction("AdminHome");
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult AddUniversity()
        {
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult AddUniversity(String uniname)
        {
            University university=new University
            {
                UniversityName = uniname
            };
            UniversityController.AdminPost(university);
            return RedirectToAction("AdminHome");
        }
    }
}
