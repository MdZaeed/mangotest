﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MongoDB.Driver;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class ResgisterController : ApiController
    {
        private readonly MongoDatabase _db1 = new MongoContext().GetDb();

        [ResponseType(typeof(UserViewModel))]
        public IHttpActionResult Post(UserPostModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UserTemp userTemp = UserTemp.ReturnUserTemp(user);
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            collection.Insert(userTemp);

            return CreatedAtRoute("DefaultApi", new { id = userTemp.UserId }, user);
        }
    }
}
