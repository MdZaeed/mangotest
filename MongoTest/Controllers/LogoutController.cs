﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class LogoutController : ApiController
    {
        public LoginResultModel Post(LoginResultModel loginResultModel1)
        {
            if (Tools.GetUserType(Request.Headers) != "Admin" && Tools.GetUserType(Request.Headers) != "User")
            {
                return null;
            }
            var loginResultModel = new LoginResultModel
            {
                Success = "True",
                Message = StringCipher.Decrypt(loginResultModel1.Message)
            };

            return loginResultModel;
        }
    }
}
