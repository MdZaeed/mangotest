﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Driver;
using MongoTest.Models;

namespace MongoTest.Controllers
{
    public class LoginController : ApiController
    {
        private static readonly MongoDatabase _db1 = new MongoContext().GetDb();

        [HttpPost]
        public LoginResultModel Post(LoginModel loginModel)
        {
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => (veh1.UserId == loginModel.Username || veh1.Email == loginModel.Email) && (veh1.Password == loginModel.Password)).Select(beh1 => beh1).ToList();
            LoginResultModel loginResultModel = new LoginResultModel();
            if (vu.Count != 0)
            {
                loginResultModel.Success = "true";
                var tt = vu[0].Usertype + "," + vu[0].UserId;
                loginResultModel.Message = StringCipher.Encrypt(tt);
            }
            else
            {
                loginResultModel.Success = "false";
                loginResultModel.Message = "UserName and Password Combination is wrong";
            }

            return loginResultModel;
        }

        public static String Receive(String emailOrUserName,String passwrod)
        {
            var collection = _db1.GetCollection<UserTemp>("UserCollection");
            var ve = collection.FindAll().ToList();
            var vu = ve.Where(veh1 => (veh1.UserId == emailOrUserName || veh1.Email == emailOrUserName) && (veh1.Password == passwrod) && (veh1.Usertype.Equals("Admin"))).Select(beh1 => beh1).ToList();
            LoginResultModel loginResultModel = new LoginResultModel();
            if (vu.Count != 0)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }
}
