﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MongoTest.Models
{
    public class LocationGetModel
    {
        [JsonProperty("vehicle_id")]
        public String vehicle_id { get; set; }
        [JsonProperty("latitude")]
        public String latitude { get; set; }
        [JsonProperty("longitude")]
        public String longitude { get; set; }
        [JsonProperty("Time")]
        public String Time { get; set; }

        public Location TransformToLocation()
        {
            var location=new Location
            {
                Latitude = latitude,
                Longitude = longitude,
                Time = "1",
                VehicleId = vehicle_id,
                LocationId = "100",
            };

            return location;
        }

        public Location TransformWithLatLngTransformationLocation()
        {
            var location = new Location
            {
                Latitude = TransormLatitude(latitude),
                Longitude = TransormLatitude(longitude),
                Time = "1",
                VehicleId = vehicle_id,
                LocationId = "100"
            };

            return location;
        }

        public String TransormLatitude(String latitudePar)
        {
            String integerNum="", fraction="";
            for (int i = 0; i < latitudePar.Length; i++)
            {
                if (i == 0 || i == 1)
                {
                    integerNum = integerNum + latitudePar[i];
                }else
                {
                    fraction = fraction + latitudePar[i];
                }
            }

            Double fractionD = Double.Parse(fraction) / 60;
            Double newNum = Int32.Parse(integerNum) + fractionD;
            string newLatitude = newNum + "";
            return newLatitude;
        }

        public String TransormLongitude(String longitudePar)
        {
            String integerNum = "", fraction = "";
            for (int i = 0; i < longitudePar.Length; i++)
            {
                if (i == 1 || i == 2)
                {
                    integerNum = integerNum + longitudePar[i];
                }
                else if (i != 5)
                {
                    fraction = fraction + longitudePar[i];
                }
            }

            Double fractionD = Int32.Parse(fraction) % 60;
            Double newNum = Int32.Parse(integerNum) + fractionD;
            string newLatitude = newNum + "";
            return newLatitude;
        }
    }
}