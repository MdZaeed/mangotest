﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoTest.Models
{
    public class LoginResultModel
    {
        public string Success { get; set; }
        public string Message { get; set; }
    }
}