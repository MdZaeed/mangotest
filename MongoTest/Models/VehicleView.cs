﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;

namespace MongoTest.Models
{
    public class VehicleView
    {
        public ObjectId Id { get; set; }
        public string DriverName { get; set; }
        public string Routes { get; set; }
        public string ActiveTime { get; set; }
        public string UniversityName { get; set; }
        public string VehicleId { get; set; }
        public string IsFavorite { get; set; }
        public string RouteCoords { get; set; }
        public string LocatorType { get; set; }

        public Vehicle TransformToVehicle()
        {
            var vehicle = new Vehicle();
            vehicle.DriverName = DriverName;
            vehicle.Routes = Routes;
            vehicle.ActiveTime = ActiveTime;
            vehicle.RouteCoords = RouteCoords;
            vehicle.LoactorType = LocatorType;
            return vehicle;
        }
    }
}