﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace MongoTest.Models
{
    public class UserPostModel
    {
        public string FullName { get; set; }
        public string UniversityName { get; set; }
        public string Department { get; set; }
        public string Institute { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Fid { get; set; }
        public string Password { get; set; }
    }

}