﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MongoTest.Models
{
    public class LocationRealGetRsponse
    {
        [JsonProperty("locations")]
        public List<LocationGetModel> locations { get; set; }
    }
}