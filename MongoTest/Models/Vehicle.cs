﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;

namespace MongoTest.Models
{
    public class Vehicle
    {
        public ObjectId Id { get; set; }
        public string DriverName { get; set; }
        public string Routes { get; set; }
        public string ActiveTime { get; set; }
        public string UniversityId { get; set; }
        public string VehicleId { get; set; }
        public string RouteCoords { get; set; }
        public string LoactorType { get; set; }

        public VehicleView AssignWithViewModel()
        {
            VehicleView vehicleView = new VehicleView();
            vehicleView.DriverName = DriverName;
            vehicleView.Routes = Routes;
            vehicleView.ActiveTime = ActiveTime;
            vehicleView.VehicleId = VehicleId;
            vehicleView.RouteCoords = RouteCoords;
            vehicleView.LocatorType = LoactorType;
            return vehicleView;
        }

    }
}