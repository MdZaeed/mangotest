﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;

namespace MongoTest.Models
{
    public class BookStore
    {
        public ObjectId Id { get; set; }
        public List<Book> Books { get; set; } 
    }
}