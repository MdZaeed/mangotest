﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoTest.Models
{
    public class University
    {
        public ObjectId Id { get; set; }
        public string UniversityName { get; set; }
        public string UniversityId { get; set; }
    }
}