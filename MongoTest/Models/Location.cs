﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MongoTest.Models
{
    public class Location
    {
        public ObjectId Id { get; set; }
        public string VehicleId { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string Time { get; set; }
        public string LocationId { get; set; }

    }
}