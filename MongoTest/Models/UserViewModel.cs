﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace MongoTest.Models
{
    public class UserViewModel
    {
        public string FullName { get; set; }
        public string UniversityName { get; set; }
        public string Department { get; set; }
        public string Institute { get; set; }
        public string UserId { get; set; }
        public string FavoriteIds { get; set; }
        public string Usertype { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Fid { get; set; }

        public static UserViewModel Transform(UserTemp user)
        {
            UserViewModel userViewModel = new UserViewModel
            {
                FullName = user.FullName,
                Department = user.Department,
                Institute = user.Institute,
                UserId = user.UserId,
                FavoriteIds = user.FavoriteIds,
                Usertype = user.Usertype,
                Username = user.Username,
                Email = user.Email,
                Fid = user.Fid
            };

            MongoDatabase db1 = new MongoContext().GetDb();

            var universityCollection = db1.GetCollection<University>("UniversityCollection");
            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityId == user.UniversityId).Select(uni => uni).ToList();
            userViewModel.UniversityName = foo[0].UniversityName;

            return userViewModel;
        }
    }

}