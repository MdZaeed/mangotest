﻿using MongoDB.Bson;

namespace MongoTest.Models
{
    public class Book
    {
        public ObjectId Id { get; set; }
        public string BookTitle { get; set; }
        public string Auther { get; set; }
        public string Category { get; set; }
        public string ISBN { get; set; }
    }
}