﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace MongoTest.Models
{
    public class Tools
    {
        public static string GetUserType(HttpRequestHeaders httpRequestHeaders)
        {
            string usertype = "";
            char[] delimiterChars = { ',' };
            IEnumerable<string> headervalues;
            try
            {
                httpRequestHeaders.TryGetValues("Auth-key", out headervalues);
                usertype = headervalues.FirstOrDefault();
            }
            catch (Exception)
            {
                return "";
            }
            usertype = StringCipher.Decrypt(usertype);
            string[] parts=usertype.Split(delimiterChars);
            usertype = parts[0];
            return usertype;
        }

        public static string GetUserId(HttpRequestHeaders httpRequestHeaders)
        {
            string usertype = "";
            char[] delimiterChars = { ',' };
            IEnumerable<string> headervalues;
            try
            {
                httpRequestHeaders.TryGetValues("Auth-key", out headervalues);
                usertype = headervalues.FirstOrDefault();
            }
            catch (Exception)
            {
                return "";
            }
            usertype = StringCipher.Decrypt(usertype);
            string[] parts = usertype.Split(delimiterChars);
            usertype = parts[1];
            return usertype;
        }
    }
}