﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoTest.Models
{
    public class UserTemp
    {
        public ObjectId Id { get; set; }
        public string FullName { get; set; }
        public string UniversityId { get; set; }
        public string Department { get; set; }
        public string Institute { get; set; }
        public string UserId { get; set; }
        public string FavoriteIds { get; set; }
        public string Usertype { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Fid { get; set; }

        public static UserTemp ReturnUserTemp(UserPostModel userViewModel)
        {
            UserTemp userTemp = new UserTemp
            {
                FullName = userViewModel.FullName,
                Department = userViewModel.Department,
                Institute = userViewModel.Institute,
                FavoriteIds = "",
                Usertype = "User",
                Username = userViewModel.Username,
                Password = userViewModel.Password,
                Email = userViewModel.Email,
                Fid = userViewModel.Fid
            };

            MongoDatabase db1 = new MongoContext().GetDb();

            var universityCollection = db1.GetCollection<University>("UniversityCollection");
            var collLis = universityCollection.FindAll().ToList();
            var foo = collLis.Where(uni => uni.UniversityName == userViewModel.UniversityName).Select(uni => uni).ToList();
            userTemp.UniversityId = foo[0].UniversityId;

            var collection = db1.GetCollection<UserTemp>("UserCollection");

            var tt = collection.FindAll().ToList();
            var gg = tt[tt.Count - 1].UserId;

            userTemp.UserId = (int.Parse(gg) + 1) + "";
            
            return userTemp;
        }
    }
}